const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  output: {
    library: 'BookmarkEditor',
    libraryTarget: 'umd',
  },
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader',
        }),
      },
    ],
  },
  externals: {
    react: 'react',
    'react-dom': 'react-dom',
  },
  plugins: [
    new ExtractTextPlugin({
      filename: './css/bookmark-editor.css',
    }),
  ],
};
