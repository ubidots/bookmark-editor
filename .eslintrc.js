module.exports = {
  "extends": "airbnb",
  "rules": {
    "react/jsx-filename-extension": [1, { "extensions": [".js", ".jsx"] }],
    "max-len": [2, 120, 2, { "ignoreUrls": true }],
    "no-underscore-dangle": "off"
  },
  "env": {
    "browser": true
  }
};
