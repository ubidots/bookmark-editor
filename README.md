# Bookmark Editor

This is a React Component to create an easy, extensible and good-looking editor that lets the user insert keywords into
a textarea while mantaining metadata available to the developer to make anything they want with that.

## Getting started

### How to install it?

This project is [available at npm](https://www.npmjs.com/package/bookmark-editor) and you can install by typing the following command:

    yarn add bookmark-editor

Or if you use npm:

    npm install --save bookmark-editor

### How to import it into my project?

In the file where you want to use this component, you can import it with:

`import BookmarkEditor from 'bookmark-editor';`

And you should also import the default CSS stylesheet:

`import 'bookmark-editor/dist/css/default.css';`


## Usage
Require the CodeMirror component and render it with JSX:

```javascript
import React from 'react';
import BookmarkEditor from 'bookmark-editor';

import '../dist/css/bookmark-editor.css';

function App() {
  const initialBookmarks = {
    hi: "I'm a bookmark",
  };

  const handleChange = (value) => {
    console.log(value);
  };

  return <BookmarkEditor value="Initial value {{hi}}" initialBookmarks={initialBookmarks} onChange={handleChange} />;
}

render(<App />, document.getElementById('app'));

```

For a more complex example, clone the repository and then run:
```bash
  yarn install && yarn example

  # or

  npm install && npm yarn example

```

After that, go to <http://localhost:8000>

## Properties

| Prop | Type | Required | Default | Description | Example |
| ---- | ---- | -------- | ------- | ----------- | --------|
| **value** | `string` | `true` |  | It works as an initial value due to the bookmark manage its own state. | `Say {{hello_world}}!`
| **initialBookmarks** | `Object` |  | `{}` | It's an object which includes the bookmarks used in the initial editor value. | `{ hello_world: 'Hello world' }` |
| **className** | `string` |  |`null`|  |  |
| **options** | `Object` |  | `{}` | Options passed to the CodeMirror instance. | `{ extraKeys: { Tab: false } }` |
| **lineNumbers** | `boolean` |  | `true` | If the editor should show the line numbers. |  |
| **onChange** | `function` |  | `() => {}` | Called when a change is made. Params: `(value:string) => {}` |  |
| **icon** | `node|element|string` |  | `'v'` | The icon  which will be shown in the bookmark list button. | `<MyIcon />`
| **showItems** | `boolean` |  | `true` | Show or hide the bookmarks list. |  |
| **items** | `array[object]` |  | `[]` | Bookmark list elements. | `[{ key: 'bookmark-1', name: 'Bookmark 1', description: 'Bookmark description', isEditable: true }]` |
| **containerStyle** | `object` |  | `{}` | Inline styles for the editor container. |  |
| **connectorCharacter** | `string` |  | `';'` | The character that separates each line break. |  |
| **focus** | `boolean` |  | `true` | Give the editor focus. |  |
| **disabled** | `boolean` |  | `false` | Disable the editor. |  |
| **lineBreakCharacter** | `string` |  | `null` | Explicitly set the line separator for the editor. |  |
| **bookmarkValidator** | `function` |  | `undefined` | When the bookmark is editable, this function allow validate the new key of the bookmark. Params: `(value:string) => {}` |  |
| **initialBookmarkElement** | `function` |  | `undefined` | Function for render the initial bookmarks. Params: `(data:object) => {}` | `(data) => <span id={data.id}>data.label</span>` |
| **beforeChangeHandlers** | `object{function}` |  | `{}` | An object with functions that will be called in the `beforeChange` editor event. Each function will be called with CodeMirror instance as the first argument and as a second argument, the change object. For more information about it go to [beforeChange event](https://codemirror.net/doc/manual.html#event_beforeChange). Functions params: `(cm:object, changeObj:object) => {}` | `{ normalize: (cm, changeObject) => {} }`



### Bookmark list - Object structure
The object structure of each item in items property must have the next keys:

- **key** (*`string`*): The identifier for the bookmark; It must be unique in the list.
- **name** (*`string`*): Friendly name for the bookmark. It will be shown in the bookmark list and when the bookmark is selected.
- **description** (*`string`*): A short description for the bookmark. It will be shown in the bookmark list.
- **isEditable** (*`boolean`*): It allows the bookmark edition in the CodeMirror editor.

### Before change handler example
Here has an example of a `beforeChangeHandlers` which not allow typing white spaces:

```javascript
import React from 'react';
import BookmarkEditor from 'bookmark-editor';

import '../dist/css/bookmark-editor.css';

const normalize = (_, changeObj) => {
  const { origin } = changeObj;
  if (origin !== '@ignore') {
    const {
      text = [], from, to,
    } = changeObj;

    const normalizedText = text.map(val => val.replace(/\s/g, ''));
    changeObj.update(from, to, normalizedText, origin);
  }
}

function App() {
  const beforeChangeHandlers = { normalize };

  const initialBookmarks = {
    hi: "I'm a bookmark",
  };

  const handleChange = (value) => {
    console.log(value);
  };

  return (
    <BookmarkEditor
      value="Initialvalue{{hi}}"
      initialBookmarks={initialBookmarks}
      onChange={handleChange}
      beforeChangeHandlers={beforeChangeHandlers}
    />
  );
}

render(<App />, document.getElementById('app'));
```

## Deployment

To deploy this project
1.  update the `package.json` file with the new version.
2.  update the `CHANGELOG.md` file with the new version.
3.  build de project 
```bash
  yarn build
```
4. commit the changes with the new version.

## License
MIT
