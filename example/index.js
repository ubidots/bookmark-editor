import React, { PureComponent } from 'react';
import { render } from 'react-dom';
import BookmarkEditor from '../src';
import '../dist/css/bookmark-editor.css';

class App extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      disabled: false,
    };

    this.bookmarkRef = React.createRef();

    this.items = [
      {
        name: 'Variable 1', description: 'This is the Variable 1', key: 'variable-1', isEditable: false,
      },
      {
        name: 'Variable 2', description: 'This is the Variable 2', key: 'variable-2', isEditable: true,
      },
      { name: 'Variable 3', description: 'This is the Variable 3', key: 'variable-3' },
      { name: 'Variable 4', description: 'This is the Variable 4', key: 'variable-4' },
      { name: 'Variable 5', description: 'This is the Variable 5', key: 'variable-5' },
    ];

    this.getData = this.getData.bind(this);
    this.disabledEditor = this.disabledEditor.bind(this);
    this.addElementToEditor = this.addElementToEditor.bind(this);
  }

  getData() {
    console.log(this.bookmarkRef.current.getCode());
  }

  addElementToEditor(textToAdd) {
    const elementContainer = children => (
      <span
        style={{
          background: '#187DC4',
          color: '#FFFFFF',
          padding: '5px 10px',
          margin: '0 5px',
          whiteSpace: 'nowrap',
        }}
        data-variable={textToAdd}
      >
        {children}
      </span>
    );

    this.bookmarkRef.current.addNew(textToAdd, elementContainer);
  }

  disabledEditor() {
    this.setState(prevState => ({ disabled: !prevState.disabled }));
  }

  render() {
    return (
      <div>
        <div>
          <BookmarkEditor
            ref={this.bookmarkRef}
            lineNumbers={false}
            items={this.items}
            showItems
            disabled={this.state.disabled}
          />
        </div>
        <button onClick={this.getData}>Get data</button>
        <button onClick={this.disabledEditor}>Disabled/Enabled</button>
      </div>
    );
  }
}

export default App;

render(<App />, document.getElementById('app'));
