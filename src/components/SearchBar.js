import React from 'react';

const SearchBar = ({ onChange }) => (
  <input
    type="text"
    placeholder="Search"
    className="editor-choices-container--input"
    onChange={onChange}
  />
);

export default SearchBar;
