import React from 'react';

import SearchBar from './SearchBar';
import ItemList from './ItemList';

const BookmarkList = ({
  items, innerRef, onSearch, openUp, onItemClick,
}) => (
  <div className="editor-choices-container" style={{ bottom: openUp ? 45 : null, top: !openUp ? 45 : null }}>
    {!openUp && <SearchBar onChange={onSearch} />}
    <ItemList innerRef={innerRef} items={items} onClick={onItemClick} />
    {openUp && <SearchBar onChange={onSearch} />}
  </div>
);

export default BookmarkList;
