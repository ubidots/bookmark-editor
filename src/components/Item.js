import React from 'react';

const Item = ({
  id, name, description, onClick, isEditable = false
}) => (
  <li className="choice-item">
    <a role="button" tabIndex="0" onClick={() => { onClick({ id, name, description, isEditable }); }}>
      <span className="choice-item--title">{name}</span>
      <span title={description}>{description}</span>
    </a>
  </li>
);

export default Item;
