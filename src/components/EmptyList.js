import React from 'react';

const EmptyList = () => (
  <li className="choice-item">
    <a role="button" tabIndex="0">
      <span className="choice-item--title">No items found</span>
    </a>
  </li>
);

export default EmptyList;
