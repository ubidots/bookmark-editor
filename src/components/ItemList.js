import React from 'react';
import Item from './Item';
import EmptyList from './EmptyList';

const ItemList = ({ innerRef, items, onClick }) => (
  <ul className="choices" ref={innerRef}>
    {items.map(item => <Item id={item.key} {...item} onClick={onClick} />)}
    {!items.length && <EmptyList />}
  </ul>
);

export default ItemList;
