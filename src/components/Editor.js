import PropTypes from 'prop-types';
import { render } from 'react-dom';
import React, { Component } from 'react';
import CodeMirror from 'react-codemirror';

import getDefaultBookmark from '../utils/getDefaultBookmark';

import 'codemirror/lib/codemirror.css';

import './Editor.css';
import BookmarkList from './BookmarkList';

/**
 * Order in ascending the added bookmarks by the character position.
 * This is intended to simplify the string modification.
 * @param {Array} markedSpans - The array of bookmarks to be organized.
 * @returns {Array} The bookmarks organized by character position in ascending way.
 */
const orderMarkedSpans = function orderMarkedSpans(markedSpans) {
  if (!Array.isArray(markedSpans)) {
    return [];
  }

  return [...markedSpans].filter(span => typeof span.marker.widgetNode !== typeof undefined).sort((act, next) => act.from - next.from);
};

class BookmarkEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchItem: '',
      showPicker: false,
    };

    this.hasFocus = false;
    this.choicesContainerRef = null;
    this.bookmarkEditor = React.createRef();

    this.addNew = this.addNew.bind(this);
    this.moveCursor = this.moveCursor.bind(this);
    this.getOptions = this.getOptions.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.filterItems = this.filterItems.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.togglePicker = this.togglePicker.bind(this);
    this.handleItemSearch = this.handleItemSearch.bind(this);
    this._initializeEditor = this._initializeEditor.bind(this);
    this.handleBeforeChange = this.handleBeforeChange.bind(this);
    this._addBookmarkOnPosition = this._addBookmarkOnPosition.bind(this);
  }

  /**
   * When the component mounts it should add the CodeMirror instance into a local attribute.
   */
  componentDidMount() {
    this._codeMirror = this.reactCodeMirror.getCodeMirror();
    this._codeMirror.setSize('100%', '100%');
    const { value, initialBookmarks } = this.props;
    if (value && initialBookmarks) {
      setTimeout(() => {
        this._initializeEditor();
      }, 1);
    }


    const { beforeChangeHandlers } = this.props;
    if (Object.keys(beforeChangeHandlers).length > 0) {
      this._codeMirror.on('beforeChange', this.handleBeforeChange);
    }

    document.addEventListener('click', this.handleClick);
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value && this.props.value !== this.getCode() && this.props.initialBookmarks) {
      setTimeout(() => {
        this._initializeEditor();
      }, 1);
    }
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClick);
  }

  /**
   * Obtain the text inside of the editor and modify it to add the values of the bookmarks.
   * @returns {String} - The text inside the editor.
   */
  getCode() {
    const textLines = [];

    this._codeMirror.eachLine((line) => {
      const orderedSpans = orderMarkedSpans(line.markedSpans);
      let indexOffset = 0;
      let lineText = line.text;

      orderedSpans.forEach((span) => {
        const variableKey = 'variable' in (span.marker.replacedWith.dataset) ? 'variable' : 'key';
        const spanValue = `{{${span.marker.replacedWith.dataset[variableKey]}}}`;
        const initialText = lineText.substr(0, span.from + indexOffset);
        const finalText = lineText.substr((span.from + 1) + indexOffset);

        lineText = `${initialText}${spanValue}${finalText}`;

        indexOffset += spanValue.length - 1;
      });

      textLines.push(lineText);
    });

    return textLines.join(this.props.connectorCharacter);
  }

  getOptions() {
    return {
      ...this.props.options,
      lineWrapping: true,
      lineNumbers: this.props.lineNumbers,
      mode: 'text/plain',
      readOnly: this.props.disabled ? 'nocursor' : false,
      lineSeparator: this.props.lineBreakCharacter,
    };
  }

  /**
   * Close Bookmark list when the user make click outside
   * @param {*} ev - Event object
   */
  handleClick(ev) {
    if (this.bookmarkEditor.current && !this.bookmarkEditor.current.contains(ev.target)) {
      this.moveCursor(false);
      this.setState({ showPicker: false });
    }
  }

  /**
   * Initialize the editor with the passed bookmarks from the properties.
   */
  _initializeEditor() {
    const initialValue = this.props.value.replace(/(\{\{\w.*?\}\})/g, '');

    this._codeMirror.setValue(initialValue);

    this.props.value.split(this.props.connectorCharacter).forEach((line, index) => {
      let modifiableLine = line;
      const bookmarks = line.split(/(\{\{\w.*?\}\})/g).filter(d => d.startsWith('{{'));

      bookmarks.forEach((bookmark) => {
        const bookmarkRaw = bookmark.replace(/[}{}]/g, '');
        const position = modifiableLine.indexOf(bookmark);

        const bookmarkFunction = typeof this.props.initialBookmarkElement === 'function' ? this.props.initialBookmarkElement : getDefaultBookmark;
        const label = this.props.initialBookmarks[bookmarkRaw];
        const bookmarkInfo = this.props.items.find((item) => {
          const variableName = item.key.split('|')[0];
          return bookmarkRaw.startsWith(variableName);
        }) || {};
        const elementContainer = bookmarkFunction({
          id: bookmarkRaw, label, name: label, callback: this.handleChange, isEditing: this.hasFocus, validator: this.props.bookmarkValidator, isEditable: bookmarkInfo.isEditable,
        });

        modifiableLine = modifiableLine.replace(bookmark, ' ');
        this._addBookmarkOnPosition({ line: index, ch: position }, { line: index, ch: position + 1 }, elementContainer);
      });
    });

    const actualPosition = this._codeMirror.getLine(this._codeMirror.lastLine()).length;

    const finalPosition = {
      line: this._codeMirror.lastLine(),
      ch: actualPosition + 1,
    };

    this._codeMirror.setCursor(finalPosition);
  }

  /**
   * Add the bookmark in the editor from the actualPosition to the finalPosition.
   * @param {Object} actualPosition The starting position in which the bookmark will be added.
   * @param {Object} finalPosition The ending position in which the bookmark will be added.
   * @param {Node} elementToAdd The React element that will be added as a Bookmark.
   */
  _addBookmarkOnPosition(actualPosition, finalPosition, elementToAdd) {
    const domContainer = document.createElement('span');
    domContainer.textContent = '';

    this._codeMirror.replaceRange(' ', actualPosition, undefined, '@ignore');

    this._codeMirror.markText(actualPosition, finalPosition, {
      replacedWith: render(elementToAdd, domContainer),
    });

    if (this.props.focus) {
      this._codeMirror.focus();
    }

    this._codeMirror.setCursor(finalPosition);
    this.handleChange();
  }

  /**
   * It takes the actual position of the cursor in the editor and adds the bookmark in that position.
   * @param {Node} elementToAdd The element to be added to the editor.
   */
  addNew(elementToAdd, element = null) {
    if (this.props.disabled) return;

    let defaultMessage = getDefaultBookmark({
      ...elementToAdd, callback: this.handleChange, isEditing: this.hasFocus, validator: this.props.bookmarkValidator,
    });
    const actualPosition = this._codeMirror.getCursor();
    const finalPosition = {
      ...this._codeMirror.getCursor(),
      ch: actualPosition.ch + 1,
    };

    if (element) {
      defaultMessage = element(elementToAdd);
    }

    this._addBookmarkOnPosition(actualPosition, finalPosition, defaultMessage);
    this.setState({ searchItem: '' }, () => { this._codeMirror.focus(); this.togglePicker(); });
  }

  handleChange(ev, information) {
    if (typeof information !== typeof undefined && information.origin === '@ignore') return;

    const text = this.getCode();
    if (
      information &&
      information.origin === '+input' &&
      information.text.length === 2
    ) {
      this._codeMirror.setCursor({
        line: information.from.line + 1,
        ch: 0,
      });
    }
    this.props.onChange(text);
  }

  /**
   * Change editor size container and move the focus to a visible line
   * @param {boolean} listIsOpen - If the bookmarks list is open
   */
  moveCursor(listIsOpen) {
    if (listIsOpen) {
      const { offsetHeight } = this.bookmarkEditor.current;
      this.bookmarkEditor.current.querySelector('div[class="CodeMirror-lines"]').style.height = `${offsetHeight}px`;
      this.bookmarkEditor.current.querySelector('div[class="CodeMirror-code"]').style.height = '1000px';

      const cursorCoors = this._codeMirror.cursorCoords(true, 'local');
      this._codeMirror.scrollTo(0, cursorCoors.top);
    } else {
      this.bookmarkEditor.current.querySelector('div[class="CodeMirror-lines"]').style.height = '';
      this.bookmarkEditor.current.querySelector('div[class="CodeMirror-code"]').style.height = '';
    }
  }

  togglePicker() {
    if (this.props.disabled) return;

    const willOpen = !this.state.showPicker;
    this.moveCursor(willOpen);

    this.setState({ showPicker: willOpen }, () => {
      if (willOpen && this.choicesContainerRef !== null) {
        const topContainerPosition = this.choicesContainerRef.getBoundingClientRect().top + 10;
        const parentHeight = this.bookmarkEditor.current.parentElement.getBoundingClientRect().top + this.bookmarkEditor.current.parentElement.getBoundingClientRect().height;
        this.choicesContainerRef.style.maxHeight = `${parentHeight - topContainerPosition}px`;
      }
    });
  }

  handleItemSearch(ev) {
    const { value } = ev.target;
    this.setState(() => ({ searchItem: value }));
  }

  filterItems() {
    const { searchItem } = this.state;
    return this.props.items.filter(item => item.name.toLowerCase().includes(searchItem.toLowerCase()));
  }

  /**
   * Allows to change or cancel a CodeMirror change event
   * @param {object} cm CodeMirror instance
   * @param {object} changeObj object has from, to, and text properties, as with the "change" event
   */
  handleBeforeChange(cm, changeObj) {
    const { beforeChangeHandlers } = this.props;
    Object.keys(beforeChangeHandlers).forEach((key) => {
      const handler = beforeChangeHandlers[key];
      if (typeof handler === 'function') {
        handler(cm, changeObj);
      }
    });
  }

  render() {
    const combinedClass = `editor-codemirror ${this.props.className} ${this.props.disabled && 'disabled'}`;
    const filteredItems = this.filterItems();
    const options = { ...this.getOptions() };

    return (
      <div
        className="editor-container"
        ref={this.bookmarkEditor}
        style={this.props.containerStyle}
      >
        {this.props.openUp && this.state.showPicker && (<BookmarkList innerRef={(container) => { this.choicesContainerRef = container; }} items={filteredItems} onSearch={this.handleItemSearch} openUp onItemClick={this.addNew} />)}
        <CodeMirror
          ref={(reactCodeMirror) => { this.reactCodeMirror = reactCodeMirror; }}
          options={options}
          className={combinedClass}
          onChange={this.handleChange}
        />

        {this.props.showItems && (
          <button className="editor-insert-button" onClick={this.togglePicker} type="button">
            {this.props.icon}
          </button>
        )}
        {!this.props.openUp && this.state.showPicker && (<BookmarkList innerRef={(container) => { this.choicesContainerRef = container; }} items={filteredItems} onSearch={this.handleItemSearch} onItemClick={this.addNew} />)}
      </div>
    );
  }
}

BookmarkEditor.propTypes = {
  value: PropTypes.string.isRequired,
  initialBookmarks: PropTypes.objectOf(PropTypes.string),
  className: PropTypes.string,
  options: PropTypes.objectOf(PropTypes.any),
  lineNumbers: PropTypes.bool,
  onChange: PropTypes.func,
  icon: PropTypes.oneOfType([PropTypes.node, PropTypes.element, PropTypes.string]),
  showItems: PropTypes.bool,
  items: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string,
    key: PropTypes.string,
    isEditable: PropTypes.bool,
  })),
  containerStyle: PropTypes.objectOf(PropTypes.any),
  connectorCharacter: PropTypes.string,
  focus: PropTypes.bool,
  disabled: PropTypes.bool,
  lineBreakCharacter: PropTypes.string,
  bookmarkValidator: PropTypes.func,
  initialBookmarkElement: PropTypes.func,
  beforeChangeHandlers: PropTypes.objectOf(PropTypes.func),
};

BookmarkEditor.defaultProps = {
  className: null,
  options: {},
  lineNumbers: true,
  onChange: () => { },
  icon: 'v',
  showItems: false,
  items: [],
  containerStyle: {},
  connectorCharacter: ';',
  focus: true,
  disabled: false,
  lineBreakCharacter: null,
  bookmarkValidator: undefined,
  initialBookmarkElement: undefined,
  beforeChangeHandlers: {},
  initialBookmarks: {},
};

export default BookmarkEditor;
