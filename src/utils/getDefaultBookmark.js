import React from 'react';

const bookmarkStatus = (bookmark, status) => {
  const styles = {
    default: {
      border: '',
      background: '#121F45',
      color: '#ffffff',
    },
    error: {
      border: '1px solid #e67c7c',
      background: '#ffffff',
      color: '#e67c7c',
    },
    editing: {
      border: '1px solid #c4c4c4',
      background: '#ffffff',
      color: '#5e5e5e',
    },
  };

  const style = styles[status];
  bookmark.style.border = style.border;
  bookmark.style.background = style.background;
  bookmark.style.color = style.color;
  if (status === 'error') bookmark.textContent = bookmark.textContent;
};

const defaultBookmark = ({
  name, id, description, callback, validator, isEditable = false
}) => {
  const containerStyle = {
    background: '#121F45',
    borderRadius: 20,
    color: '#FFFFFF',
    display: 'inline-block',
    fontSize: 12,
    padding: '3px 10px',
    margin: '0 3px',
    outline: 0,
    whiteSpace: 'nowrap',
  };

  const modifyItself = (ev) => {
    ev.target.setAttribute('data-key', ev.target.textContent);
    callback(null, '');
  };

  const setAsEditable = (ev) => {
    ev.preventDefault();
    ev.stopPropagation();


    bookmarkStatus(ev.target, 'editing');
    ev.target.setAttribute('contenteditable', true);
    ev.target.textContent = ev.target.dataset.key;
    ev.target.focus();
  };

  const removeContentEditable = (ev) => {
    ev.preventDefault();
    ev.stopPropagation();

    if (typeof validator === 'function' && !validator(ev.target.textContent)) {
      bookmarkStatus(ev.target, 'error');
      return;
    }

    bookmarkStatus(ev.target, 'default');
    ev.target.removeAttribute('contenteditable');
  };

  const handleKeyPress = (ev) => {
    const keyPressed = ev.which || ev.keyCode;

    if (keyPressed === 13 || keyPressed === 27) {
      ev.preventDefault();
      if (typeof validator === 'function' && !validator(ev.target.textContent)) {
        bookmarkStatus(ev.target, 'error');
        return;
      }

      removeContentEditable(ev);
    }
  };

  let editProps = {};
  if (isEditable) {
    editProps = {
      onInput:modifyItself,
      onDoubleClick: setAsEditable,
      onBlur: removeContentEditable,
      onKeyDown: handleKeyPress,
    };
  }

  return (
    <div
      style={containerStyle}
      data-key={id}
      title={description}      
      role="button"
      tabIndex={0}
      {...editProps}
    >
      {name}
    </div>
  );
};

export default defaultBookmark;
