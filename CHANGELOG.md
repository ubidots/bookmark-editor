# Changelog

All notable changes of this project will be documented on this file.

## [0.0.3] - 2018-04-09

- New way to send custom styles to React-Codemirror via className prop.
- New way to send custom options via options prop.
- New shortcut to display line numbers via lineNumbers prop.

## [0.0.4] - 2021-08-18

- Support to timestamp format using "or" operator (|)

## [0.0.5] - 2022-02-23

- Update state of initialValue

## [0.0.6] - 2022-02-23

- Fix bug: Update state of initial value
- allow to handle brake lines

## [0.0.7] - 2022-11-28
- Fix bug: fix infinite loop when using onChange prop